#include "SetMatrix.h"
#include<QScreen>
#include<QGuiApplication>
SetMatrix::SetMatrix(QObject *parent):QAbstractListModel(parent)
{
    windowHeight=250;
    windowWidth=600;
}
Matrix* SetMatrix::currentMatrix(){
    return _setMatrix[currentndex+1];

}
QModelIndex SetMatrix::index(int row, int column, const QModelIndex &parent) const {

// return QModelIndex();
return createIndex(row, column, nullptr);
}
int SetMatrix::rowCount(const QModelIndex &parent) const{
    return _setMatrix.size();

};
int SetMatrix::columnCount(const QModelIndex &parent) const{
    if(_setMatrix.size()!=0)
        return _setMatrix[0]->readCols();
    return 0;

};
void SetMatrix::push_back(Matrix* matrix){

    _setMatrix.insert(_setMatrix.begin()+currentndex+1,matrix);
    currentndex=-1;
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    int height = screenGeometry.height();
    int width = screenGeometry.width();
    if(windowHeight<700)
        windowHeight+=450;
    else windowHeight=height;
    if(windowHeight>height) windowHeight=height;
    if(windowWidth>1000) windowWidth=width;
    if(_setMatrix.size()!=1 && windowWidth<1000)
        windowWidth+=450;

    emit windowHeightChanged(windowHeight);
    emit windowWidthChanged(windowWidth);

}
//в данном случае, роль будет просто число, на которое указывет индекс матрицы
QVariant SetMatrix::data(const QModelIndex &index, int role) const{
    qDebug()<<"index "<<index;

};
bool SetMatrix::setData(const QModelIndex &index, const QVariant &value, int role){



};
/*
1 1 0   1 3 4
2 2 0   2 5 6
3 3 4   3 7 8


*/
Matrix* SetMatrix::operation(int i1,int i2,QString type)try
{

   qDebug()<<i1<<" "<<i2<<" "<<type;
    resultMatrix=new Matrix();
    auto matrix_1=_setMatrix[i1];
    auto matrix_2=_setMatrix[i2];

    if(type=="+" || type=="-"){

    resultMatrix->setCols(matrix_1->readCols());
    resultMatrix->setRows(matrix_1->readRows());
    resultMatrix->matrix_initialize();
    if(matrix_1->readCols()!=matrix_2->readCols() && matrix_1->readRows()!=matrix_2->readRows()){
        throw("matrix1.rows and cols dont equal matrix2.rows and cols");
        return nullptr;
    }
    for(int i=0;i<resultMatrix->size();i++)
        for(int j=0;j<resultMatrix->readCols();j++)
        resultMatrix->getMatrix()[i][j]=type=="+" ? (matrix_1->getMatrix())[i][j]+(matrix_2->getMatrix())[i][j] :
           (matrix_1->getMatrix())[i][j]-(matrix_2->getMatrix())[i][j];


    }
   if(type =="*"){

       resultMatrix->setCols(matrix_1->readCols());
       resultMatrix->setRows(matrix_2->readRows());
       resultMatrix->matrix_initialize();
       if(matrix_1->readCols()!=matrix_2->readRows()){
              throw("matrix1.cols!=matrix2.rows");
              return nullptr;
       }
       auto sum=0;
       for(int i=0;i<resultMatrix->readCols();i++)
           for(int k=0;k<resultMatrix->readRows();k++){
                for(int j=0;j<resultMatrix->readCols();j++){

                    sum+=(matrix_1->getMatrix())[j][k]*(matrix_2->getMatrix())[k][j];
           }
            resultMatrix->getMatrix()[i][k]=sum;
            sum=0;
   }
}

    return resultMatrix;

}
catch(const char* e){
   qFatal(e);

}
