#ifndef SETMATRIX_H
#define SETMATRIX_H
#include<QObject>
#include<QAbstractListModel>
#include"Matrix.h"
#include<deque>




class SetMatrix:public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int windowWidth READ readWindowWidth NOTIFY windowWidthChanged)
    Q_PROPERTY(int windowHeight READ readWindowHeight NOTIFY windowHeightChanged)
    Q_PROPERTY( Matrix* currentMatrix READ getMatrix)
public:

    explicit SetMatrix(QObject *parent = nullptr);
    Matrix* currentMatrix();
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    int columnCount(const QModelIndex &parent) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

public slots:
    Matrix* operation(int i1,int i2,QString type);
    int size(){
        return _setMatrix.size();
    }
    void writeCurrentIndex(int value){
        currentndex=value;
    }
    int readCurrentIndex(){

        return currentndex;
    }
    Matrix* getMatrix(){
        currentndex++;
        return _setMatrix[currentndex];
    }
    void push_back(Matrix* matrix);
    int readWindowWidth(){

        return windowWidth;
    }
    int readWindowHeight(){

        return windowHeight;
    }
    Matrix* getByIdx(int index){
        return  _setMatrix[index];
    }
signals:
    void windowHeightChanged(int new_Height);
    void windowWidthChanged(int new_Width);
private:
    std::deque<Matrix*> _setMatrix;
    int windowWidth=0;
    int windowHeight=0;
    int currentndex=-1;
    Matrix* resultMatrix;

};

#endif // SETMATRIX_H
