import QtQuick 2.12
import QtQuick.Window 2.12
import QtQml 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import set_matrix 1.0
Window {
    id:root
    property var matrixRef:SetMatrix{}
    width: matrixRef.windowWidth
    height: matrixRef.windowHeight
    visible: true
    title: qsTr("Matrix Calc")
    maximumHeight: height
    maximumWidth: width
    minimumHeight: height
    minimumWidth: width
    Connections{
        target:matrixRef
        onWindowHeightChanged:{
            root.maximumHeight=new_Height
            root.height=new_Height
        }
        onWindowWidthChanged:{
            root.maximumWidth=new_Width
            root.width=new_Width
        }
    }
    StackView{
        id:loader
        anchors.fill:parent
        initialItem: "QuestionAddMatrix.qml"
        pushEnter: Transition {
               PropertyAnimation {
                   property: "opacity"
                   from: 0
                   to:1
                   duration: 200
               }
           }
           pushExit: Transition {
               PropertyAnimation {
                   property: "opacity"
                   from: 1
                   to:0
                   duration: 200
               }
           }
           popEnter: Transition {
               PropertyAnimation {
                   property: "opacity"
                   from: 0
                   to:1
                   duration: 200
               }
           }
           popExit: Transition {
               PropertyAnimation {
                   property: "opacity"
                   from: 1
                   to:0
                   duration: 200
               }
           }
    }


}
