import QtQuick 2.0
import QtQuick.Window 2.12
import QtQml 2.12
import QtQuick.Controls 2.12
Rectangle {
    id:root
    width: parent.width
    height: parent.height
    visible: true
    property var size:matrixRef.size()
    property var matrix_Index:0
    ListView{
        anchors.leftMargin:10
        anchors.fill:parent
        model:matrixRef
        height: parent.height
        spacing: 50
        orientation: Qt.Horizontal
        onCurrentIndexChanged:{
           matrix_Index=currentIndex
        }
        delegate:
            Rectangle{
            id:matrixRectangle
            property var matrix:matrixRef.currentMatrix
            anchors.topMargin: 20
            width:root.width/(1.5*size)
            height:root.height/2

            Text{
                anchors.top:parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text:"Заполните матрицу значениями"
                font.pixelSize: parent.width/15

            }
            Column{
                anchors.left:gridView.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 10
                spacing:10
            Repeater{
                model:["+","-","*"]
            Button{
                id:plusButton
                visible:matrixRef.readCurrentIndex()!=size-1
                width:30
                height:30
                text:modelData
                font.pixelSize: 20
                onClicked: {
                    matrixRef.operation(matrix_Index,matrix_Index+1,modelData)
                    console.log("click")
                }
            }
            }
            }

            GridView{
                id:gridView
                anchors.topMargin: 40
                width:parent.width
                height:parent.height
                cellHeight: (parent.height)/matrix.rows
                cellWidth: (parent.width)/matrix.cols
                model:matrix.rows*matrix.cols
                anchors.fill:parent
                delegate:
                    Rectangle{
                    id:columns
                    width:(parent.width)/matrix.cols
                    height:parent.height/matrix.rows
                    border.color:"black"
                    TextEdit{
                        id:columnsText
                        anchors.fill:parent
                        text:matrix.get_ij_position(index)
                        Connections{
                            target:matrix
                            onRowsChanged:  columnsText.text=matrix.get_ij_position(index);
                        }
                        font.pixelSize: 20
                        onTextChanged: {
                            matrix.write_to_ij_position(index,columnsText.text)
                        }
                    }
                }

            }

        CheckBox{
            id:transponizeCheckBox
            text:"Транспонировать"
            anchors.top:gridView.bottom
            anchors.topMargin:50
            anchors.left:gridView.left
            onClicked:matrix.transponize()
        }
        StackView{
            id:new_loader
            anchors.fill:parent
        }
        CheckBox{
            id:appendMatrixeCheckBox
            text:"Добавить новую матрицу"
            anchors.top:transponizeCheckBox.bottom
            anchors.topMargin:20
            anchors.left:transponizeCheckBox.left
            onClicked:{
                matrixRef.writeCurrentIndex(matrix_Index )
                new_loader.push("loadNewMatrix.qml")
            }
        }
        Button{
            id:detButton
            anchors.top: appendMatrixeCheckBox.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 20
            width:parent.width
            height:60
            visible:matrix.rows==matrix.cols
            text:"Посчитать  определитель"
            font.pixelSize: 20
            onClicked: {
                if(detButton.text=="Посчитать  определитель"){determinateTimer.running=true;text=matrix.getDeterminant();}

            }
        }
        Timer{
            id:determinateTimer
            repeat:false; running: false;interval: 2000
            onTriggered: detButton.text="Посчитать  определитель"
        }

        }

    }

}
