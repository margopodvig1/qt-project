#ifndef MATRIX_H
#define MATRIX_H
#include<QDebug>
#include <QObject>
#include<QFile>








class Matrix : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int rows READ readRows WRITE setRows NOTIFY rowsChanged)
    Q_PROPERTY(int cols READ readCols WRITE setCols NOTIFY colsChanged)
public:
    explicit Matrix(QObject *parent = nullptr);
    int size(){
        return rows;
    }
    int readRows(){

        return this->rows;

    }

    int readCols(){

        return this->cols;

    }
    void matrix_initialize(){
        emit rowsChanged();
        emit colsChanged();
        if(_Matrix.size()!=0)
            return;
        _Matrix.reserve(rows);
        auto new_cols=std::vector<int>(cols,0);
        for(int i=0;i<rows;i++)
            _Matrix.push_back(new_cols);


    }
   std::vector<std::vector<int>> reconfigureMatrix(int j,std::vector<std::vector<int>> matrix);
    int getDeterminant_toQml(std::vector<std::vector<int>> matrix);
    std::vector<int> operator [](int b){
        return _Matrix[b];
    };

public slots:
    void transponize();
      int get_ij_position(int index){
          qDebug()<<index<<" "<<index/cols<<" "<<index%cols;
        return  _Matrix[index/cols][index%cols];

    }
    void write_to_ij_position(int index,int var){
        _Matrix[index/cols][index%cols]=var;
     }
    void readMatrixFromFile(QString str);
    int getDeterminant(){

        return  getDeterminant_toQml(_Matrix);
    };
    void setRows(int row){

        rows=row;
        qDebug()<<"row is"<< row;

    }
    void setCols(int col){

        cols=col;
        qDebug()<<"col is"<< col;
        matrix_initialize();

    }
    std::vector<std::vector<int>> getMatrix(){
        return this->_Matrix;
    }

signals:
    void rowsChanged();
    void colsChanged();
private:
    int rows;
    int cols;
    std::vector<std::vector<int>> _Matrix;

};

#endif // MATRIX_H
