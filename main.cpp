#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include"Matrix.h"
#include"SetMatrix.h"
int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<Matrix>("matrix",1,0,"Matrix");
    qRegisterMetaType<Matrix*>();
    qmlRegisterType<SetMatrix>("set_matrix",1,0,"SetMatrix");
    qRegisterMetaType<SetMatrix*>();

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
