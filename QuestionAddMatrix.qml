import QtQuick 2.0
import QtQml 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import matrix 1.0
Rectangle{
    id:root
    width:parent.width
    height: parent.height
    anchors.fill:parent
    property var matrix: Matrix{}
    Text{
        id:textChoose
        text:"Введите количество строк и столбцов"
        anchors.top:parent.top
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 20
    }
    CheckBox{
        id:readCheckBox
        anchors.top:rows.bottom
        anchors.left: rows.left
        anchors.topMargin: 10
        checked: false
       text: qsTr("Считать матрицу из файла")
       onClicked:if(checkState) openFileDialog.visible=true

    }
    FileDialog {
           id: openFileDialog
           title: "Please choose a file"
           nameFilters: ["Text files (*.txt)"]
           onAccepted:  {console.log("You chose: " + openFileDialog.fileUrls);
               textEdit.text=openFileDialog.fileUrl;
               textEdit.text=textEdit.text.slice(8,textEdit.text.length)
                matrix.readMatrixFromFile(textEdit.text);

           }
       }
    Rectangle{
        id:fileText
        width:parent.width-40
        height:40
        anchors.top:readCheckBox.bottom
        anchors.topMargin:10
        anchors.left:readCheckBox.left
        visible:readCheckBox.checkState
        border.color:"black"
    TextArea {
          id: textEdit
          text:""
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.verticalCenter: parent.verticalCenter
          anchors.right:parent.right
          anchors.left: parent.left
          font.pixelSize: 20
      }
    }

    Rectangle{
        id:rows
        width:100
        height:30
        anchors.top:textChoose.bottom
        anchors.left:parent.left
        anchors.leftMargin:20
        anchors.topMargin: 40
        border.color:"black"
        TextEdit{
            id:rowsText
            anchors.verticalCenter:parent.verticalCenter
            text:matrix.rows
            anchors.fill:parent
            anchors.topMargin:5
            anchors.leftMargin:10
            font.pixelSize: 20
            onTextChanged:{
                if(rowsText.text.length>2){
                    rowsText.text=rowsText.text.slice(0,2)
                    cursorPosition = 2
                }

            }
        }
    }
    Rectangle{
        id:columns
        width:100
        height:30
        anchors.top:textChoose.bottom
        anchors.right:textChoose.right
        anchors.topMargin: 40
        border.color:"black"
        TextEdit{
            id:columnsText
            anchors.fill:parent
            text:matrix.cols
            font.pixelSize: 20
            anchors.topMargin:5
            anchors.leftMargin:5
            onTextChanged:{
                if(columnsText.text.length>2){
                    columnsText.text=columnsText.text.slice(0,2)
                    cursorPosition = 2
                }
            }

        }
    }

    Button{
        id:okButton
        anchors.bottom:parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin:5
        width:parent.width/4
        height:20
        palette.button : "red"
        Text{
            text:"ОК"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter:parent.verticalCenter
            color:"white"
        }
        onClicked:{
            matrix.setRows(rowsText.text);
            matrix.setCols(columnsText.text);
            matrixRef.push_back(matrix);
            loader.push("matrix.qml")
            //root.visible=false
        }
    }


}
