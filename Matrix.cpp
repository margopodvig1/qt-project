#include "Matrix.h"

Matrix::Matrix(QObject *parent) : QObject(parent)
{
    rows=0;
    cols=0;

}

void Matrix::readMatrixFromFile(QString str){

    QFile file(str);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug()<<"not open! " << str;
        return;
    }
    int i=0;
    int j=0;
    QTextStream in(&file);
    while (!in.atEnd()) {
      QString l = in.readLine();
      QStringList line=l.split(' ');
      j=line.size();
      i++;
      std::vector<int> new_row{};
      for(auto& i:line)
          new_row.push_back(i.toInt());
      _Matrix.push_back(new_row);
      }
    rows=i;
    cols=j;
    emit rowsChanged();
    emit colsChanged();
};
void  Matrix::transponize(){
    std::swap(rows,cols);
    Matrix new_matrix;
    new_matrix.rows=rows;
    new_matrix.cols=cols;
    new_matrix.matrix_initialize();
   for(int i=0;i<rows;i++)
       for(int j=0;j<cols;j++)
           new_matrix._Matrix[i][j]=_Matrix[j][i];

    _Matrix=new_matrix._Matrix;
    emit rowsChanged();
    emit colsChanged();
}
std::vector<std::vector<int>> Matrix::reconfigureMatrix(int j,std::vector<std::vector<int>> matrix){

    for(int i=0;i<matrix.size();i++)
        matrix[i].erase(matrix[i].begin());
    matrix.erase(matrix.begin()+j);
    return matrix;
}
int Matrix::getDeterminant_toQml(std::vector<std::vector<int>> matrix){

   if(matrix.size()==1)  return matrix[0][0];
   int sum=0;
   for(int i=0;i<matrix.size();++i)
   {
       if(i%2==0)
            sum+=matrix[i][0]*getDeterminant_toQml(reconfigureMatrix(i,matrix));
       else
            sum-=matrix[i][0]*getDeterminant_toQml(reconfigureMatrix(i,matrix));
   }
   return sum;
}
